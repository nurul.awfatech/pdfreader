import pdfplumber
import json
from datetime import datetime

class MyDate():
    checking_format = "%d/%m/%y"
    new_format = "%Y-%m-%d"
    def __init__(self,date):
        self.date=date

    def is_valid_date(date_string, format):
        try:
            datetime.strptime(date_string, format)
            return True
        except ValueError:
            return False
    
class PdfReaderBank():
    def __init__(self,file,bank,date):
        self.file = file  # Path and name of pdf bank statement
        self.bank = bank  # Type of bank statement => MBB,BIMB
        self.date = date  # Date of bank statement closing
        self.result = ''  # will consist
        self.data = ''

    def read_mbb_business(self):
        aa=self.date.split("-")
        date_year=aa[0]
        trap_month=f"/{aa[1]} "  # create trap for month date as mbb format "30/07 "
        page_num = row_num = total_data = count = total_ct = total_dt = 0  # Just declaration
        trans_data = []  # Declare a storage 
        # print(">>",date_year,">>",trap_month)  # Debug
        #trap_month="/07 "  # Hardcode first to capture row that consist date as mbb format "30/07 "
        #date_year="2021"

        # Open the PDF file
        with pdfplumber.open(self.file) as pdf:
            # Iterate through each page of the PDF and append to variable
            raw_text = ''
            for page in pdf.pages:
                raw_text += page.extract_text()
            # end:for
        # end:with
        # print(raw_text)  # Debug just print the raw text
        
        # Split the raw text into lines
        all_line = raw_text.splitlines()  # Put all text into list/array of list_all_line
        # Iterate through each line of text
        for one_line in all_line:

            # To capture the beginning balance value in the file
            if "BEGINNING BALANCE" in one_line:
                #print(one_line)  # Eg. BEGINNING BALANCE 7,388.69
                a1 = one_line.split()
                beginning_bal = a1[len(a1)-1]
                beginning_bal = beginning_bal.replace(",","")  # Remove ","" from eg. 1,000.00
                beginning_bal = format(float(beginning_bal), '.2f')
                #print(">>",beginning_bal)

            # To capture the endding balance value in the file
            if "ENDING BALANCE :" in one_line:
                #print(one_line)  # Eg. ENDING BALANCE : 5,946.79
                a2 = one_line.split()
                ending_bal = a2[len(a2)-1]
                ending_bal = ending_bal.replace(",","")  # Remove ","" from eg. 1,000.00
                ending_bal = format(float(ending_bal), '.2f')
                #print(">>",ending_bal)

            # To capture the transaction value in the file
            if trap_month in one_line:  # Only capture line that consist dateEg. "/07 " to capture 30/07 TRANSFER TO A/C 40.00+ 7,402.79
                array_txt = one_line.split()  # Split into array so easy to process/clean by field
                len1 = len(array_txt)
                date = array_txt[0] + "/" + date_year  # Mbb store date just eg 31/08 so need to add year
                raw_amount = array_txt[len1-2]  # Eg. 1,200.50+ => consist + or -
                len2 = len(raw_amount)  # Get the len so later can remove the sign + or - 
                sign = raw_amount[len2-1]  # We need to keep the variable sign to know it debit or credit
                amount = raw_amount.replace(sign,"")  # Remove sign
                amount = amount.replace(",","")  # Remove ","" from eg. 1,000.00
                amount = format(float(amount), '.2f')

                # Convert the string to a datetime object
                #date_object = datetime.strptime(date, "%d/%m/%y")
                # Convert the datetime object to the desired format
                #date = date_object.strftime("%Y-%m-%d")
                temp=date.split("/")
                newdate=f"{temp[2]}-{temp[1]}-{temp[0]}"
                date=newdate

                if sign == "+":
                    total_ct = total_ct + float(amount)
                if sign == "-":
                    total_dt = total_dt + float(amount)
                # amount = float("{:.2f}".format(amount))
                # Now process or construt the description. Desc will need to used later to verify / matching process
                # Need to take only particur array and remove unnessasary array
                i=0
                info=''
                for i in range(1,len1-2):
                    # print(i,">>",array_txt[i]) # Debug
                    info=info + " " + array_txt[i]
                # end:for
                info=info.strip() # Trim
               
                i=0
                ignoremode=0
                for i in range(1,200): #200 coz just for loop until start new table trans
                    extra_info=all_line[row_num+i]
                    if trap_month in extra_info:
                        # Means start new trans
                        #print ("found new trans")
                        break
                    if "ENDING BALANCE :" in extra_info:
                        # Means end of all trans
                        #print ("new file ")
                        break
                    if "BAKI LEGAR" in extra_info:
                        #print ("end trans table")
                        ignoremode=1
                        continue
                        # Means end of page so need to loop until found start page 
                    if "ENTRY DATE" in extra_info:
                        #print ("new trans table")
                        ignoremode=0
                        continue
                        # Found back new page 
                    if ignoremode == 0:
                        info=info + " " + extra_info

                # Now we got the clean of Data, Amount, Sign and Information
                # Debug
                count += 1 # Just a debug counter
                # print(count,">>",date,">>",amount,">>",sign,">>",info,">>",one_line,">>",len1,">>",len2)
                # print(f"{count} >> {newdate} >> {amount} >> {sign} >> {info}")
                
                # Store result in Dict format so later can convert to json
                result = {
                    "date": date,
                    "amount": float(amount),
                    "sign": sign,
                    "description": info
                    }
                # print(json.dumps(result))  # Debug
                trans_data.append(result)  # Store result valid transactions
                total_data += 1 # counter of valid trans
            # end:if
            row_num += 1 # counter all_line
        # end:for

        #print(json.dumps(trans_data))  # Debug

        # Got issue "total_debit": 483859.22000000003, "total_credit": 575428.0000000001
        # So need to formated. Normall happen when there operation on the data
        response = {
                    "file": self.file,
                    "bank": self.bank,
                    "date": self.date,
                    "beginning_balance": float(beginning_bal),
                    "ending_balance": float(ending_bal),
                    "total_debit": float(f"{total_dt:.2f}"),
                    "total_credit": float(f"{total_ct:.2f}"),
                    "total_data": total_data,
                    "data": trans_data
                    }
        
        self.result = 'Success'
        self.data = response
        return True
    # end: read_mbb_business
    

def main():
    print('Starting the process')
    reader = PdfReaderBank('upload/sample_mbb_business.pdf','MBB_BUSINESS','2021-07-31')
    #Mbb business and personal different litte bit the format.
    if reader.read_mbb_business():
        print("File: " + reader.file)
        print("Bank: " + reader.bank)
        print("Date: " + reader.date)
        print("Result: " + reader.result)
        print("")
        #res=json.dumps(recon.__dict__)  # Dont used. Just set the info. Convert Class to Dict so can convert to json
        #print(res)
        print(json.dumps(reader.data))
    else:
        print("Process PdfReaderBank failed")

if __name__ == '__main__': 
    main()
